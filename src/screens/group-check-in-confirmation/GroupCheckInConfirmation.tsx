import React, { useEffect, useState } from "react";
import {
  SafeAreaView,
  View,
  Pressable,
  ScrollView,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import styles from "./GroupCheckInConfirmationStyles";
import { StatusBar } from "react-native";
import { Header } from "../../components/basics/header";
import { HiraginoKakuText } from "../../components/StyledText";
import { Footer } from "../../components/basics/footer";
import { Button } from "../../components/basics/Button";
import { MaterialIcons, Feather } from "@expo/vector-icons";
import { colors } from "../../styles/color";
import Completion from "../completion/Completion";
import { NavigationProp, useRoute } from "@react-navigation/native";

type Props = {
  navigation: NavigationProp<any, any>;
};

export const GroupCheckInConfirmation = ({ navigation }: Props) => {
  const [bodyScrollEnabled, setBodyScrollEnabled] = useState(false);
  const [scrollEnabled, setScrollEnabled] = useState(false);
  const [isModalVisible, setModalVisible] = useState(false);
  const [isShowCorrectedBadge, setIsShowCorrectedBadge] = useState(false);

  const onLayoutHandler = (e: any) => {
    var { height } = e.nativeEvent.layout;
    if (height > 250) {
      setScrollEnabled(true);
    }
  };

  // prev next buttons' height
  const [topPosition, setTopPosition] = useState(0);

  const onLayoutBtnHandler = (e: any) => {
    let skipBtnPosition = hp("51%") / 2 - hp("3.2%");
    setTopPosition(skipBtnPosition);
    var { height } = e.nativeEvent.layout;
    if (height > 400) {
      setBodyScrollEnabled(true);
    }
    else {
      setBodyScrollEnabled(false)
    }
  };

  let [selectedId, setSelectedId] = useState<string>("1");
  const [isLeftCircleVisible, setIsLeftCircleVisible] = useState(false);
  const [isRightCircleVisible, setIsRightCircleVisible] = useState(true);

  interface DataItem {
    id: string;
    text: string;
    relationship: string;
    name: string;
    namekana: string;
    dob: string;
    gender: string;
    postalcode: string;
    address: string;
  }
  const data = [
    {
      id: "1",
      text: "出茂 太郎",
      relationship: "",
      name: "出茂　太郎",
      namekana: "イズモ　タロウ",
      dob: "2000年01月02日",
      gender: "男性",
      postalcode: "515-0004",
      address:
        "三重県松阪市なんとか町11-2　マンション名102",
    },
    {
      id: "2",
      text: "出茂 二郎",
      relationship: "きょうだい",
      name: "出茂　太郎",
      namekana: "イズモ　タロウ",
      dob: "2001年03月02日",
      gender: "男性",
      postalcode: "515-0004",
      address:
        "三重県松阪市なんとか町11-2　マンション名102あああああああああああああああああああああああああああああああああああああああああああああああ",
    },
    {
      id: "3",
      text: "出茂 太郎ああああああああ",
      relationship: "むすこ",
      name: "田中　武",
      namekana: "タケシ　タナカ",
      dob: "2000年01月02日",
      gender: "男性",
      postalcode: "515-0004",
      address:
        "三重県松阪市なんとか町11-2　マンション名102あああああああああああああああああ",
    },
    {
      id: "4",
      text: "出茂 太郎ああああ",
      relationship: "むすめ",
      name: "田中　桜",
      namekana: "サクラ　タナカ",
      dob: "2000年01月02日",
      gender: "女性",
      postalcode: "515-0004",
      address:
        "三重県松阪市なんとか町11-2　マンション名102あああああああああああああああああ",
    },
    {
      id: "5",
      text: "出茂 太郎ああああああああああああ",
      relationship: "つま",
      name: "鈴木　恵美",
      namekana: "エミ　スズキ",
      dob: "1975年05月05日",
      gender: "女性",
      postalcode: "515-0004",
      address:
        "三重県松阪市なんとか町11-2　マンション名102あああああああああああああああああ",
    },
    {
      id: "6",
      text: "出茂 太郎ああああ",
      relationship: "せたいぬし",
      name: "中村　雪",
      namekana: "ユキ　ナカムラ",
      dob: "1970年01月02日",
      gender: "男性",
      postalcode: "515-0004",
      address:
        "三重県松阪市なんとか町11-2　マンション名102あああああああああああああああああ",
    },
    {
      id: "7",
      text: "出茂 太郎ああああ",
      relationship: "むすこ",
      name: "田中　春樹",
      namekana: "ハルキ　タナカ",
      dob: "2005年01月12日",
      gender: "男性",
      postalcode: "515-0004",
      address:
        "三重県松阪市なんとか町11-2　マンション名102ああああああああああああああああああああああ",
    },
  ];

  const handleDivSelect = (id: any) => {
    setSelectedId(id);
    if (id == 1) {
      setIsLeftCircleVisible(false);
      setIsRightCircleVisible(true);
    } else if (id === data[data.length - 1].id) {
      setIsLeftCircleVisible(true);
      setIsRightCircleVisible(false);
    } else {
      setIsLeftCircleVisible(true);
      setIsRightCircleVisible(true);
    }
  };

  //Render Scroll Item
  const renderScrollItem = ({ item }: { item: DataItem }) => (
    <Pressable onPress={() => handleDivSelect(item.id)}>
      <View
        style={
          selectedId === item.id ? styles.btnSideDivActive : styles.btnSideDiv
        }
      >
        <View style={styles.scrollContent}>
          <HiraginoKakuText style={styles.count}>{item.id}.</HiraginoKakuText>
          <HiraginoKakuText
            numberOfLines={1}
            style={[
              styles.sideText,
              isReturn !== "true"
                ? styles.sideTextWidth
                : styles.sideTextWidthCorrected,
            ]}
            normal
          >
            {item.text}
          </HiraginoKakuText>
        </View>

        {isShowCorrectedBadge && (
          <View style={styles.correctedBadge}>
            <HiraginoKakuText style={styles.correctedText}>
              修正済
            </HiraginoKakuText>
          </View>
        )}
      </View>
    </Pressable>
  );

  //Previous Person Info
  const OnPreviousCirclePress = () => {
    let currentIndex = data.findIndex((item) => item.id === selectedId);
    if (currentIndex > 0) {
      let previousIndex = data[currentIndex - 1].id;
      currentIndex = parseInt(previousIndex);
      setSelectedId(previousIndex);
    }
    if (currentIndex == 1) {
      setIsLeftCircleVisible(false);
      setIsRightCircleVisible(true);
    } else if (currentIndex === parseInt(data[data.length - 1].id)) {
      setIsLeftCircleVisible(true);
      setIsRightCircleVisible(false);
    } else {
      setIsLeftCircleVisible(true);
      setIsRightCircleVisible(true);
    }
  };

  //Next Person Info
  const OnNextCirclePress = () => {
    let currentIndex = data.findIndex((item) => item.id === selectedId);
    if (currentIndex < data.length - 1) {
      let nextIndex = data[currentIndex + 1].id;
      currentIndex = parseInt(nextIndex);
      setSelectedId(nextIndex);
    }
    if (currentIndex == 1) {
      setIsLeftCircleVisible(false);
      setIsRightCircleVisible(true);
    } else if (currentIndex === parseInt(data[data.length - 1].id)) {
      setIsLeftCircleVisible(true);
      setIsRightCircleVisible(false);
    } else {
      setIsLeftCircleVisible(true);
      setIsRightCircleVisible(true);
    }
  };

  //Render Info Item
  const renderInfoItem = () => {
    const selectedData = data.find((item) => item.id === selectedId);

    return (
      <View>
        {selectedId !== "1" && (
          <View style={styles.row}>
            <View style={styles.rowContent}>
              <View style={styles.firstContent}>
                <HiraginoKakuText style={styles.innerBodyBoldText}>
                  あなたとの関係
                </HiraginoKakuText>
              </View>

              <View
                style={
                  isReturn !== "true"
                    ? styles.secondContent
                    : styles.secondContentCorrected
                }
              >
                <HiraginoKakuText style={styles.innerBodyText} normal>
                  {selectedData?.relationship}
                </HiraginoKakuText>
              </View>
            </View>
            {isShowCorrectedBadge && (
              <View style={styles.correctedBadge}>
                <HiraginoKakuText style={styles.correctedText}>
                  修正済
                </HiraginoKakuText>
              </View>
            )}
          </View>
        )}
        <View style={styles.row}>
          <View style={styles.rowContent}>
            <View style={styles.firstContent}>
              <HiraginoKakuText style={styles.innerBodyBoldText}>
                お名前
              </HiraginoKakuText>
            </View>
            <View
              style={
                isReturn !== "true"
                  ? styles.secondContent
                  : styles.secondContentCorrected
              }
            >
              <HiraginoKakuText style={styles.innerBodyText} normal>
                {selectedData?.name}
              </HiraginoKakuText>
            </View>
          </View>
          {isShowCorrectedBadge && (
            <View style={styles.correctedBadge}>
              <HiraginoKakuText style={styles.correctedText}>
                修正済
              </HiraginoKakuText>
            </View>
          )}
        </View>

        <View style={styles.row}>
          <View style={styles.rowContent}>
            <View style={styles.firstContent}>
              <HiraginoKakuText style={styles.innerBodyBoldText}>
                お名前（カナ）
              </HiraginoKakuText>
            </View>
            <View
              style={
                isReturn !== "true"
                  ? styles.secondContent
                  : styles.secondContentCorrected
              }
            >
              <HiraginoKakuText style={styles.innerBodyText} normal>
                {selectedData?.namekana}
              </HiraginoKakuText>
            </View>
          </View>
          {isShowCorrectedBadge && (
            <View style={styles.correctedBadge}>
              <HiraginoKakuText style={styles.correctedText}>
                修正済
              </HiraginoKakuText>
            </View>
          )}
        </View>

        <View style={styles.row}>
          <View style={styles.rowContent}>
            <View style={styles.firstContent}>
              <HiraginoKakuText style={styles.innerBodyBoldText}>
                生年月日
              </HiraginoKakuText>
            </View>
            <View
              style={
                isReturn !== "true"
                  ? styles.secondContent
                  : styles.secondContentCorrected
              }
            >
              <HiraginoKakuText style={styles.innerBodyText} normal>
                {selectedData?.dob}
              </HiraginoKakuText>
            </View>
          </View>
          {isShowCorrectedBadge && (
            <View style={styles.correctedBadge}>
              <HiraginoKakuText style={styles.correctedText}>
                修正済
              </HiraginoKakuText>
            </View>
          )}
        </View>
        {/* // 性別(Female/male/other) is optional. */}
        <View style={styles.row}>
          <View style={styles.rowContent}>
            <View style={styles.firstContent}>
              <HiraginoKakuText style={styles.innerBodyBoldText}>
                性別
              </HiraginoKakuText>
            </View>
            <View
              style={
                isReturn !== "true"
                  ? styles.secondContent
                  : styles.secondContentCorrected
              }
            >
              <HiraginoKakuText style={styles.innerBodyText} normal>
                {selectedData?.gender}
              </HiraginoKakuText>
            </View>
          </View>
          {isShowCorrectedBadge && (
            <View style={styles.correctedBadge}>
              <HiraginoKakuText style={styles.correctedText}>
                修正済
              </HiraginoKakuText>
            </View>
          )}
        </View>

        <View style={styles.row}>
          <View style={styles.rowContent}>
            <View style={styles.firstContent}>
              <HiraginoKakuText style={styles.innerBodyBoldText}>
                郵便番号
              </HiraginoKakuText>
            </View>

            <View
              style={
                isReturn !== "true"
                  ? styles.secondContent
                  : styles.secondContentCorrected
              }
            >
              <HiraginoKakuText style={styles.innerBodyText} normal>
                {selectedData?.postalcode}
              </HiraginoKakuText>
            </View>
          </View>
          {isShowCorrectedBadge && (
            <View style={styles.correctedBadge}>
              <HiraginoKakuText style={styles.correctedText}>
                修正済
              </HiraginoKakuText>
            </View>
          )}
        </View>

        <View style={styles.rowAddress}>
          <View style={styles.rowContent}>
            <View style={styles.firstContentAddress}>
              <HiraginoKakuText style={styles.innerBodyBoldText}>
                住所
              </HiraginoKakuText>
            </View>
            <View
              style={
                isReturn !== "true"
                  ? styles.secondContentAddress
                  : styles.secondContentAddressCorrected
              }
            >
              <HiraginoKakuText style={styles.innerBodyText} normal>
                {selectedData?.address}
              </HiraginoKakuText>
            </View>
          </View>
          {isShowCorrectedBadge && (
            <View style={styles.correctedBadge}>
              <HiraginoKakuText style={styles.correctedText}>
                修正済
              </HiraginoKakuText>
            </View>
          )}
        </View>
      </View>
    );
  };

  const handleCompletion = () => {
    openCompletionModal();
  };
  const route = useRoute();
  const { userId, isReturn } = route.params as {
    userId: string;
    isReturn: string;
  };

  useEffect(() => {
    if (isReturn == "true") {
      setIsShowCorrectedBadge(true);
    }
    if (isModalVisible === true) {
      let timeOut = setTimeout(() => {
        closeModal();
        navigation.navigate("SelectReceptionMethod", {
          userId: "mec",
        });
      }, 10000);
      return () => clearTimeout(timeOut);
    }
  });

  const openCompletionModal = () => {
    setModalVisible(true);
  };

  const closeModal = () => {
    setModalVisible(false);
    navigation.navigate("SelectReceptionMethod", {
      userId: "mec",
    });
  };

  const handleEdit = () => {
    navigation.navigate("GroupCheckInEdit", {
      userId: userId,
    });
  };

  const handleSelectReceptionMethod = () => {
    navigation.navigate("SelectReceptionMethod", {
      userId: "mec",
    });
  };

  return (
    <SafeAreaView style={styles.mainContainer}>
      <StatusBar barStyle="dark-content"></StatusBar>
      <Header
        titleName="受付内容確認"
        buttonName="受付をやめる"
        onPress={handleSelectReceptionMethod}
      ></Header>
      <ScrollView scrollEnabled={bodyScrollEnabled}>
        <View style={styles.bodyContainer}>
          <View style={styles.innerMainTitle}>
            <HiraginoKakuText style={styles.innerMainTitleText}>
              この内容で受付しますか？
            </HiraginoKakuText>
          </View>
          <View style={styles.outerContainer}>
            <View style={styles.sideScrollDiv}>
              <HiraginoKakuText style={styles.sideTitleText}>
                受付人数３人
              </HiraginoKakuText>
              <ScrollView
                style={styles.scrollableGroup}
                scrollEnabled={scrollEnabled}>
                <View style={styles.scrollableGp} onLayout={onLayoutHandler}>
                  {data.map((item) => (
                    <View key={item.id}>{renderScrollItem({ item })}</View>
                  ))}
                </View>
              </ScrollView>
            </View>

            <View>
              <View
                style={[
                  styles.innerBodyContainer,
                  selectedId !== "1"
                    ? styles.innerBodyContainerNot
                    : styles.innerBodyContainer,
                ]}
              >
                <View onLayout={onLayoutBtnHandler}>
                  <View style={styles.bodyTitle}>
                    <HiraginoKakuText style={styles.bodyTitleText}>
                      受付内容
                    </HiraginoKakuText>
                    <View style={styles.buttonContainer}>
                      <Button
                        text="内容を修正する"
                        type="ButtonMSecondary"
                        style={styles.btnModify}
                        icon={
                          <MaterialIcons
                            name="mode-edit"
                            size={24}
                            color={colors.primary}
                            style={styles.iconStyle}
                          />
                        }
                        iconPosition="behind"
                        onPress={handleEdit}
                      ></Button>
                    </View>
                  </View>
                  {renderInfoItem()}
                </View>
              </View>

              {isLeftCircleVisible && (
                <View
                  style={{
                    position: "absolute",
                    top: topPosition,
                    left: -26,
                  }}
                >
                  <Pressable
                    onPress={OnPreviousCirclePress}
                    style={styles.chevronLeftButton}
                  >
                    <Feather
                      name="chevron-left"
                      size={28}
                      color={colors.secondary}
                    />
                  </Pressable>
                </View>
              )}
              {isRightCircleVisible && (
                <View
                  style={{ position: "absolute", top: topPosition, right: -26 }}
                >
                  <Pressable
                    onPress={OnNextCirclePress}
                    style={styles.chevronRightButton}
                  >
                    <Feather
                      name="chevron-right"
                      size={28}
                      color={colors.secondary}
                    />
                  </Pressable>
                </View>
              )}
            </View>
          </View>
        </View>
      </ScrollView>
      <Footer
        rightButtonText="受付する"
        hasPreviousButton={false}
        showNextIcon={false}
        onPressNext={handleCompletion}
      ></Footer>
      {isModalVisible && <Completion closeModal={closeModal} />}
    </SafeAreaView>
  );
};

export default GroupCheckInConfirmation;
