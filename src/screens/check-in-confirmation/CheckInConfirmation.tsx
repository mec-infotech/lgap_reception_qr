import React, { useEffect, useState } from "react";
import { SafeAreaView, View, Dimensions, ScrollView } from "react-native";
import styles from "./CheckInConfirmationStyles";
import { StatusBar } from "react-native";
import { Header } from "../../components/basics/header";
import { HiraginoKakuText } from "../../components/StyledText";
import { Footer } from "../../components/basics/footer";
import { Button } from "../../components/basics/Button";
import { MaterialIcons } from "@expo/vector-icons";
import { colors } from "../../styles/color";
import Completion from "../completion/Completion";
import { NavigationProp, useRoute } from "@react-navigation/native";
import { format } from "date-fns";

type Props = {
  navigation: NavigationProp<any, any>;
};

type Params = {
  firstName: string,
  lastName: string,
  firstNameKana: string,
  lastNameKana: string,
  dateOfBirth: Date,
  postalCode: string,
  genderCode: string,
  address: string,
}

export const CheckInConfirmation = ({ navigation }: Props) => {
  const route = useRoute();
  let { firstName, lastName, firstNameKana, lastNameKana, dateOfBirth, postalCode, genderCode, address } = route.params as Params;

  const [isModalVisible, setModalVisible] = useState(false);
  const [scrollEnabled, setScrollEnabled] = useState(false);
  const [isShowCorrectedBadge, setIsShowCorrectedBadge] = useState(false);
  const [postCode, setPostCode] = useState("");
  const [formattedDate, setformattedDate] = useState("");
  const [genderName, setGenderName] = useState<string>('');

  const handleCompletion = () => {
    openCompletionModal();
  };
  const { userId, isReturn } = route.params as {
    userId: string;
    isReturn: string;
  };

  useEffect(() => {
    if (isReturn == "true") {
      setIsShowCorrectedBadge(true);
    }

    let formattedDate: string;

    if (dateOfBirth) {
      formattedDate = format(dateOfBirth, "yyyy年MM月dd日");
    } else {
      formattedDate = "Invalid date";
    }

    setformattedDate(formattedDate);

    setPostCode(postalCode);
    // get Gender
    const name =fetchGenderNamebyGenderCode(genderCode);
    setGenderName( name);

  }, [formattedDate, postalCode, genderCode]);
  useEffect(() => {
    if (isModalVisible === true) {
      let timeOut = setTimeout(() => {
        closeModal();
        navigation.navigate("SelectReceptionMethod", {
          userId: "mec",
        });
      }, 10000);
      return () => clearTimeout(timeOut);
    }
  });

  const openCompletionModal = () => {
    setModalVisible(true);
  };

  const closeModal = () => {
    setModalVisible(false);
    navigation.navigate("SelectReceptionMethod", {
      userId: "mec",
    });
  };

  const handleEdit = () => {
    navigation.navigate("CheckInEdit", {
      userId: "mec",
    });
  };

  const handleSelectReceptionMethod = () => {
    navigation.navigate("SelectReceptionMethod", {
      userId: "mec",
    });
  };

  const onLayoutHandler = (e: any) => {
    var { height } = e.nativeEvent.layout;

    if (height > 400) {
      setScrollEnabled(true);
    } else {
      setScrollEnabled(false);
    }
  };

  const fetchGenderNamebyGenderCode = (genderCode: string) => {
    // Define a mapping of gender codes to gender names
    const genderMap: { [key: string]: string } = {
      '0': '同意しない', // "Do not agree"
      '1': '男性',     // "Male"
      '2': '女性'      // "Female"
    };

    // Return the corresponding gender name or a default message if the code is not found
    return genderMap[genderCode] || 'Unknown gender code';
  };

  return (
    <SafeAreaView style={styles.mainContainer}>
      <StatusBar barStyle="dark-content"></StatusBar>
      <Header
        titleName="受付内容確認"
        buttonName="受付をやめる"
        onPress={handleSelectReceptionMethod}
      ></Header>
      <ScrollView scrollEnabled={scrollEnabled}>
        <View style={styles.bodyContainer}>
          <View style={styles.innerMainTitle}>
            <HiraginoKakuText style={styles.innerMainTitleText}>
              この内容で受付しますか？
            </HiraginoKakuText>
          </View>

          <View style={styles.innerBodyContainer} onLayout={onLayoutHandler}>
            <View style={styles.bodyTitle}>
              <HiraginoKakuText style={styles.bodyTitleText}>
                受付内容
              </HiraginoKakuText>
              <View style={styles.buttonContainer}>
                <Button
                  text="内容を修正する"
                  type="ButtonMSecondary"
                  style={styles.btnModify}
                  icon={
                    <MaterialIcons
                      name="mode-edit"
                      size={24}
                      color={colors.primary}
                      style={styles.iconStyle}
                    />
                  }
                  iconPosition="behind"
                  onPress={handleEdit}
                ></Button>
              </View>
            </View>
            <View style={styles.rowGroup}>
              <View style={styles.row}>
                <View style={styles.rowContent}>
                  <View style={styles.firstContent}>
                    <HiraginoKakuText style={styles.innerBodyBoldText}>
                      お名前
                    </HiraginoKakuText>
                  </View>
                  <View
                    style={
                      isReturn !== "true"
                        ? styles.secondContent
                        : styles.secondContentCorrected
                    }
                  >
                    <HiraginoKakuText style={styles.innerBodyText} normal>
                      {
                        lastName
                      }
                      {"　"}
                      {
                        firstName
                      }
                    </HiraginoKakuText>
                  </View>
                </View>
                {isShowCorrectedBadge && (
                  <View style={styles.correctedBadge}>
                    <HiraginoKakuText style={styles.correctedText}>
                      修正済
                    </HiraginoKakuText>
                  </View>
                )}
              </View>

              <View style={styles.row}>
                <View style={styles.rowContent}>
                  <View style={styles.firstContent}>
                    <HiraginoKakuText style={styles.innerBodyBoldText}>
                      お名前（カナ）
                    </HiraginoKakuText>
                  </View>
                  <View
                    style={
                      isReturn !== "true"
                        ? styles.secondContent
                        : styles.secondContentCorrected
                    }
                  >
                    <HiraginoKakuText style={styles.innerBodyText} normal>
                      {
                        lastNameKana
                      }
                      {"　"}
                      {
                        firstNameKana
                      }
                    </HiraginoKakuText>
                  </View>
                </View>
                {isShowCorrectedBadge && (
                  <View style={styles.correctedBadge}>
                    <HiraginoKakuText style={styles.correctedText}>
                      修正済
                    </HiraginoKakuText>
                  </View>
                )}
              </View>

              <View style={styles.row}>
                <View style={styles.rowContent}>
                  <View style={styles.firstContent}>
                    <HiraginoKakuText style={styles.innerBodyBoldText}>
                      生年月日
                    </HiraginoKakuText>
                  </View>
                  <View
                    style={
                      isReturn !== "true"
                        ? styles.secondContent
                        : styles.secondContentCorrected
                    }
                  >
                    <HiraginoKakuText style={styles.innerBodyText} normal>
                      {formattedDate}
                    </HiraginoKakuText>
                  </View>
                </View>
                {isShowCorrectedBadge && (
                  <View style={styles.correctedBadge}>
                    <HiraginoKakuText style={styles.correctedText}>
                      修正済
                    </HiraginoKakuText>
                  </View>
                )}
              </View>
              {/* // 性別(Female/male/other) is optional. */}
              {genderCode != "0" && (
                <View style={styles.row}>
                  <View style={styles.rowContent}>
                    <View style={styles.firstContent}>
                      <HiraginoKakuText style={styles.innerBodyBoldText}>
                        性別
                      </HiraginoKakuText>
                    </View>
                    <View
                      style={
                        isReturn !== "true"
                          ? styles.secondContent
                          : styles.secondContentCorrected
                      }
                    >
                      <HiraginoKakuText style={styles.innerBodyText} normal>
                        {genderName}
                      </HiraginoKakuText>
                    </View>
                  </View>
                  {isShowCorrectedBadge && (
                    <View style={styles.correctedBadge}>
                      <HiraginoKakuText style={styles.correctedText}>
                        修正済
                      </HiraginoKakuText>
                    </View>
                  )}
                </View>
              )}

              <View style={styles.row}>
                <View style={styles.rowContent}>
                  <View style={styles.firstContent}>
                    <HiraginoKakuText style={styles.innerBodyBoldText}>
                      郵便番号
                    </HiraginoKakuText>
                  </View>
                  <View
                    style={
                      isReturn !== "true"
                        ? styles.secondContent
                        : styles.secondContentCorrected
                    }
                  >
                    <HiraginoKakuText style={styles.innerBodyText} normal>
                      {
                        postalCode
                      }
                    </HiraginoKakuText>
                  </View>
                </View>
                {isShowCorrectedBadge && (
                  <View style={styles.correctedBadge}>
                    <HiraginoKakuText style={styles.correctedText}>
                      修正済
                    </HiraginoKakuText>
                  </View>
                )}
              </View>

              <View style={styles.rowAddress}>
                <View style={styles.rowContent}>
                  <View style={styles.firstContent}>
                    <HiraginoKakuText style={styles.innerBodyBoldText}>
                      住所
                    </HiraginoKakuText>
                  </View>
                  <View
                    style={
                      isReturn !== "true"
                        ? styles.secondContentAddress
                        : styles.secondContentAddressCorrected
                    }
                  >
                    <HiraginoKakuText style={styles.innerBodyText} normal>
                      {
                        address
                      }
                    </HiraginoKakuText>
                  </View>
                </View>
                {isShowCorrectedBadge && (
                  <View style={styles.correctedBadge}>
                    <HiraginoKakuText style={styles.correctedText}>
                      修正済
                    </HiraginoKakuText>
                  </View>
                )}
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
      <Footer
        rightButtonText="受付する"
        hasPreviousButton={false}
        showNextIcon={false}
        onPressNext={handleCompletion}
      ></Footer>
      {isModalVisible && <Completion closeModal={closeModal} />}
    </SafeAreaView>
  );
};

export default CheckInConfirmation;
