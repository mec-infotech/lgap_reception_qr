import React, { useState, useRef } from "react";
import {
  StatusBar,
  SafeAreaView,
  View,
  ScrollView,
  Pressable,
  TextInput,
  GestureResponderEvent,
  Dimensions,
  Keyboard,
  TouchableWithoutFeedback,
} from "react-native";
import styles from "./GroupCheckInEditStyles";
import { Header } from "../../components/basics/header";
import { HiraginoKakuText } from "../../components/StyledText";
import { Footer } from "../../components/basics/footer";
import { Button } from "../../components/basics/Button";
import { MaterialIcons, Entypo, Feather } from "@expo/vector-icons";
import { colors } from "../../styles/color";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { CustomCalendar } from "../../components/basics/Calendar";
import { format } from "date-fns";

const width = Dimensions.get("window").width;

interface TooltipProps {
  isVisible: boolean;
  x: number;
  y: number;
  text: string;
}

const Tooltip = ({ isVisible, x, y, text }: TooltipProps) => {
  if (!isVisible) return null;

  return (
    <View style={[styles.tooltip, { left: x - 70, top: y - 70 }]}>
      <HiraginoKakuText style={styles.tooltipText} normal>
        {text}
      </HiraginoKakuText>
      <View style={styles.arrow} />
    </View>
  );
};

interface ToastProps {
  isVisible: boolean;
  x: number;
  y: number;
  text: string;
}

const Toast = ({ isVisible, x, y, text }: ToastProps) => {
  if (!isVisible) return null;

  return (
    <View style={[styles.addresstoastContainer, { left: x - 70, top: y + 20 }]}>
      <HiraginoKakuText style={styles.toastText}>{text}</HiraginoKakuText>
    </View>
  );
};
import { NavigationProp } from "@react-navigation/native";

type Props = {
  navigation: NavigationProp<any, any>;
};
export const GroupCheckInEdit = ({ navigation }: Props) => {
  const [scrollEnabled, setScrollEnabled] = useState(false);
  const [selectedOption, setSelectedOption] = useState("");
  const [tooltipVisible, setTooltipVisible] = useState(false);
  const [tooltipX, setTooltipX] = useState(0);
  const [tooltipY, setTooltipY] = useState(0);
  const [toastVisible, setToastVisible] = useState(false);
  const [toastX, setToastX] = useState(0);
  const [toastY, setToastY] = useState(0);
  const [editScrollEnabled, setEditScrollEnabled] = useState(false);
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [firstKanaName, setFirstKanaName] = useState("");
  const [lastKanaName, setLastKanaName] = useState("");
  const [postCode, setpostCode] = useState("");
  const [address, setAddress] = useState("");

  const [firstNameErrMsg, setFirstNameErrMsg] = useState("");
  const [lastNameErrMsg, setLastNameErrMsg] = useState("");
  const [firstKanaNameErrMsg, setFirstKanaNameErrMsg] = useState("");
  const [lastKanaNameErrMsg, setLastKanaNameErrMsg] = useState("");
  const [postCodeErrMsg, setPostcodeErrMsg] = useState("");
  const [addressErrMsg, setAddressErrMsg] = useState("");
  const [isBirthDateCalendarVisible, setBirthDateCalendarVisible] = useState(false);
  const [birthDate, setBirthDate] = useState("");
  const [editContainerHeight, seteditContainerHeight] = useState(0);
  const birthDateInputRef = useRef(null);
  const birthDateRef = useRef(null);
  const calendarRef = useRef(null);
  const editContainerRef = useRef(null);

  const handlePress = (event: GestureResponderEvent) => {
    const { nativeEvent } = event;
    setTooltipVisible(true);
    setTooltipX(nativeEvent.pageX);
    setTooltipY(nativeEvent.pageY);
    setTimeout(() => setTooltipVisible(false), 2000);
    setBirthDateCalendarVisible(false);
  };

  const handleAddressPress = (event: GestureResponderEvent) => {
    const { nativeEvent } = event;
    setToastVisible(true);
    setToastX(nativeEvent.pageX);
    setToastY(nativeEvent.pageY);
    setTimeout(() => setToastVisible(false), 2000);
    setBirthDateCalendarVisible(false);
  };

  const handleSelectOption = (option: string) => {
    setSelectedOption(option);
  };

  const defaultOnPress = () => {
    setBirthDateCalendarVisible(false);
  };

  const onLayoutHandler = (e: any) => {
    var { height } = e.nativeEvent.layout;
    if (height > 350) {
      setScrollEnabled(true);
    }
  };
  const onRadioPanelHandler = () => {
    setSelectedOption("M");
  };

  const onEditLayoutHandler = (e: any) => {
    var { height } = e.nativeEvent.layout;
    seteditContainerHeight(height);
    if (height > hp("56.2%")) {
      setEditScrollEnabled(true);
    }
  };

  const [selectedDiv, setSelectedDiv] = useState<string | null>("1");
  const handleDivSelect = (divId: string) => {
    setSelectedDiv(divId);
    if (divId == "1") {
      setIsLeftCircleVisible(false);
      setIsRightCircleVisible(true);
    } else if (divId == "7") {
      setIsLeftCircleVisible(true);
      setIsRightCircleVisible(false);
    } else {
      setIsLeftCircleVisible(true);
      setIsRightCircleVisible(true);
    }
  };

  // prev next buttons' height
  const [topPosition, setTopPosition] = useState(0);

  const onLayoutBtnHandler = () => {
    let skipBtnPosition = hp("51%") / 2 - hp("3.2%");
    setTopPosition(skipBtnPosition);
  };

  //Render Edit Info Item
  const [isLeftCircleVisible, setIsLeftCircleVisible] = useState(false);
  const [isRightCircleVisible, setIsRightCircleVisible] = useState(true);
  //Previous Person Info
  const OnPreviousCirclePress = () => {
    if (selectedDiv === "2") {
      handleDivSelect("1");
    } else if (selectedDiv === "3") {
      handleDivSelect("2");
    } else if (selectedDiv === "4") {
      handleDivSelect("3");
    } else if (selectedDiv === "5") {
      handleDivSelect("4");
    } else if (selectedDiv === "6") {
      handleDivSelect("5");
    } else if (selectedDiv === "7") {
      handleDivSelect("6");
    }
    setBirthDateCalendarVisible(false);
  };

  //Next Person Info
  const OnNextCirclePress = () => {
    if (selectedDiv === "1") {
      handleDivSelect("2");
    } else if (selectedDiv === "2") {
      handleDivSelect("3");
    } else if (selectedDiv === "3") {
      handleDivSelect("4");
    } else if (selectedDiv === "4") {
      handleDivSelect("5");
    } else if (selectedDiv === "5") {
      handleDivSelect("6");
    } else if (selectedDiv === "6") {
      handleDivSelect("7");
    }
    setBirthDateCalendarVisible(false);
  };

  const handleInputChange = (text: string, fieldName: string) => {
    if (fieldName == "firstName") {
      setFirstName(text);
    } else if (fieldName == "lastName") {
      setLastName(text);
    } else if (fieldName == "firstKanaName") {
      setFirstKanaName(text);
    } else if (fieldName == "lastKanaName") {
      setLastKanaName(text);
    } else if (fieldName == "postCode") {
      setpostCode(text);
    } else if (fieldName == "address") {
      setAddress(text);
    }
  };

  const handleConfirm = () => {
    if (
      firstName == "" ||
      lastName == "" ||
      firstKanaName == "" ||
      lastKanaName == "" ||
      postCode == "" ||
      address == ""
    ) {
      if (firstName == "") {
        setFirstNameErrMsg("入力してください");
      } else {
        setFirstNameErrMsg("");
      }
      if (lastName == "") {
        setLastNameErrMsg("入力してください");
      } else {
        setLastNameErrMsg("");
      }
      if (firstKanaName == "") {
        setFirstKanaNameErrMsg("カタカナで入力してください");
      } else {
        setFirstKanaNameErrMsg("");
      }
      if (lastKanaName == "") {
        setLastKanaNameErrMsg("カタカナで入力してください");
      } else {
        setLastKanaNameErrMsg("");
      }
      if (postCode == "") {
        setPostcodeErrMsg("入力してください");
      } else {
        setPostcodeErrMsg("");
      }
      if (address == "") {
        setAddressErrMsg("入力してください");
      } else {
        setAddressErrMsg("");
      }
    } else {
      navigation.navigate("GroupCheckInConfirmation", {
        userId: "mec",
        isReturn: "true",
      });
    }
  };

  const handleSelectReceptionMethod = () => {
    navigation.navigate("SelectReceptionMethod", {
      userId: "mec",
    });
  };

  const handleBirthDateCalendarPress = (event: any) => {
    (birthDateInputRef.current as any).focus();
    setBirthDateCalendarVisible(!isBirthDateCalendarVisible);
  };

  const handleBirthDateSelect = (date: any) => {
    setBirthDate(date);
    setBirthDateCalendarVisible(false);
  };

  const closeCalendar = (event: any) => {
    if (event.nativeEvent.target != birthDateInputRef.current && event.nativeEvent.target != birthDateRef) {
      if (isBirthDateCalendarVisible) {
        setBirthDateCalendarVisible(false);
      }
    }
  };

  const onCalendarLayout = (e: any) => {
    if (calendarRef.current) {
      seteditContainerHeight(editContainerHeight + hp("8%"));
    }
  }

  return (
    <TouchableWithoutFeedback onPress={closeCalendar}>
      <SafeAreaView style={styles.mainContainer}>
        <StatusBar barStyle="dark-content"></StatusBar>
        <Header
          titleName="受付内容修正"
          buttonName="受付をやめる"
          onPress={handleSelectReceptionMethod}
        ></Header>
        <KeyboardAwareScrollView
          style={{ flex: 1, width: "100%" }}
          resetScrollToCoords={{ x: 0, y: 0 }}
          contentContainerStyle={styles.mainContainer}
          scrollEnabled={false}
        >
          <View style={[styles.bodyContainer]}>
            <View style={styles.innerMainTitle}>
              <HiraginoKakuText style={styles.innerMainTitleText}>
                受付内容を修正してください
              </HiraginoKakuText>
            </View>
            <View style={styles.outerContainer}>
              <View style={styles.sideScrollDiv}>
                <View style={styles.sideTitleDiv}>
                  <HiraginoKakuText style={styles.sideTitleText}>
                    受付人数３人
                  </HiraginoKakuText>
                </View>
                <ScrollView
                  style={styles.scrollableGroup}
                  scrollEnabled={scrollEnabled}
                >
                  <View style={styles.scrollableGp} onLayout={onLayoutHandler}>
                    <Pressable onPress={() => { handleDivSelect("1"); setBirthDateCalendarVisible(false); }}>
                      <View
                        style={
                          selectedDiv === "1"
                            ? styles.btnSideDivActive
                            : styles.btnSideDiv
                        }
                      >
                        <HiraginoKakuText style={styles.count}>
                          1.
                        </HiraginoKakuText>
                        <HiraginoKakuText
                          numberOfLines={1}
                          style={styles.sideText}
                          normal
                        >
                          出茂　太郎
                        </HiraginoKakuText>
                      </View>
                    </Pressable>
                    <Pressable onPress={() => { handleDivSelect("2"); setBirthDateCalendarVisible(false); }}>
                      <View
                        style={
                          selectedDiv === "2"
                            ? styles.btnSideDivActive
                            : styles.btnSideDiv
                        }
                      >
                        <HiraginoKakuText style={styles.count}>
                          2.
                        </HiraginoKakuText>
                        <HiraginoKakuText
                          numberOfLines={1}
                          style={styles.sideText}
                          normal
                        >
                          出茂　瑠音
                        </HiraginoKakuText>
                      </View>
                    </Pressable>
                    <Pressable onPress={() => { handleDivSelect("3"); setBirthDateCalendarVisible(false); }}>
                      <View
                        style={
                          selectedDiv === "3"
                            ? styles.btnSideDivActive
                            : styles.btnSideDiv
                        }
                      >
                        <HiraginoKakuText style={styles.count}>
                          3.
                        </HiraginoKakuText>
                        <HiraginoKakuText
                          numberOfLines={1}
                          style={styles.sideText}
                          normal
                        >
                          出茂　太郎太郎太郎
                        </HiraginoKakuText>
                      </View>
                    </Pressable>
                    <Pressable onPress={() => { handleDivSelect("4"); setBirthDateCalendarVisible(false); }}>
                      <View
                        style={
                          selectedDiv === "4"
                            ? styles.btnSideDivActive
                            : styles.btnSideDiv
                        }
                      >
                        <HiraginoKakuText style={styles.count}>
                          4.
                        </HiraginoKakuText>
                        <HiraginoKakuText
                          numberOfLines={1}
                          style={styles.sideText}
                          normal
                        >
                          出茂 太郎ああああ
                        </HiraginoKakuText>
                      </View>
                    </Pressable>
                    <Pressable onPress={() => { handleDivSelect("5"); setBirthDateCalendarVisible(false); }}>
                      <View
                        style={
                          selectedDiv === "5"
                            ? styles.btnSideDivActive
                            : styles.btnSideDiv
                        }
                      >
                        <HiraginoKakuText style={styles.count}>
                          5.
                        </HiraginoKakuText>
                        <HiraginoKakuText
                          numberOfLines={1}
                          style={styles.sideText}
                          normal
                        >
                          出茂 太郎ああああああああああああ
                        </HiraginoKakuText>
                      </View>
                    </Pressable>
                    <Pressable onPress={() => { handleDivSelect("6"); setBirthDateCalendarVisible(false); }}>
                      <View
                        style={
                          selectedDiv === "6"
                            ? styles.btnSideDivActive
                            : styles.btnSideDiv
                        }
                      >
                        <HiraginoKakuText style={styles.count}>
                          6.
                        </HiraginoKakuText>
                        <HiraginoKakuText
                          numberOfLines={1}
                          style={styles.sideText}
                          normal
                        >
                          出茂 太郎ああああ
                        </HiraginoKakuText>
                      </View>
                    </Pressable>
                    <Pressable onPress={() => { handleDivSelect("7"); setBirthDateCalendarVisible(false); }}>
                      <View
                        style={
                          selectedDiv === "7"
                            ? styles.btnSideDivActive
                            : styles.btnSideDiv
                        }
                      >
                        <HiraginoKakuText style={styles.count}>
                          7.
                        </HiraginoKakuText>
                        <HiraginoKakuText
                          numberOfLines={1}
                          style={styles.sideText}
                          normal
                        >
                          出茂 太郎ああああ
                        </HiraginoKakuText>
                      </View>
                    </Pressable>
                  </View>
                </ScrollView>
              </View>

              {selectedDiv && (
                <ScrollView
                  style={styles.editScrollContainer}
                  scrollEnabled={editScrollEnabled}
                  onLayout={onLayoutBtnHandler}
                >
                  <TouchableWithoutFeedback onPress={closeCalendar}>
                    <View ref={editContainerRef}
                      style={[styles.editContainer, (isBirthDateCalendarVisible) && { height: editContainerHeight }]}
                      onLayout={onEditLayoutHandler}
                    >
                      {selectedDiv === "2" && (
                        <View style={styles.row}>
                          <View style={styles.labelContainer}>
                            <HiraginoKakuText style={styles.labelText}>
                              あなたとの関係
                            </HiraginoKakuText>
                          </View>
                          <View style={styles.innerRowContainer}>
                            <View style={styles.rowInput}>
                              <HiraginoKakuText
                                style={styles.birthDateInputText}
                                normal
                              ></HiraginoKakuText>
                              <Pressable
                                style={styles.dropDownIconContainer}
                                onPress={defaultOnPress}
                              >
                                <Entypo
                                  name="chevron-small-down"
                                  size={24}
                                  color={colors.activeCarouselColor}
                                  style={styles.dropDownIcon}
                                />
                              </Pressable>
                            </View>
                          </View>
                        </View>
                      )}
                      <View
                        style={[
                          styles.row,
                          firstNameErrMsg != "" || lastNameErrMsg != ""
                            ? { height: "auto" }
                            : { height: hp("5.15%") },
                        ]}
                      >
                        <View style={styles.labelContainer}>
                          <HiraginoKakuText style={styles.labelText}>
                            お名前
                          </HiraginoKakuText>
                        </View>
                        <View style={styles.inputsContainer}>
                          <View
                            style={[
                              styles.inputErrContainer,
                              firstNameErrMsg != ""
                                ? { gap: hp("0.95%") }
                                : { gap: 0 },
                            ]}
                          >
                            <TextInput
                              style={[styles.input, styles.inputText]}
                              value={firstName}
                              onChangeText={(text) =>
                                handleInputChange(text, "firstName")
                              }
                              onFocus={() => setBirthDateCalendarVisible(false)}
                            />
                            {firstNameErrMsg != "" && (
                              <HiraginoKakuText style={styles.errText} normal>
                                {firstNameErrMsg}
                              </HiraginoKakuText>
                            )}
                          </View>
                          <View
                            style={[
                              styles.inputErrContainer,
                              lastNameErrMsg != ""
                                ? { gap: hp("0.95%") }
                                : { gap: 0 },
                            ]}
                          >
                            <TextInput
                              style={[styles.input, styles.inputText]}
                              value={lastName}
                              onChangeText={(text) =>
                                handleInputChange(text, "lastName")
                              }
                              onFocus={() => setBirthDateCalendarVisible(false)}
                            />
                            {lastNameErrMsg != "" && (
                              <HiraginoKakuText style={styles.errText} normal>
                                {lastNameErrMsg}
                              </HiraginoKakuText>
                            )}
                          </View>
                        </View>
                      </View>
                      <View
                        style={[
                          styles.row,
                          firstKanaNameErrMsg != "" || lastKanaNameErrMsg != ""
                            ? { height: "auto" }
                            : { height: hp("5.15%") },
                        ]}
                      >
                        <View style={styles.labelContainer}>
                          <HiraginoKakuText style={styles.labelText}>
                            お名前（カナ）
                          </HiraginoKakuText>
                        </View>
                        <View style={styles.inputsContainer}>
                          <View
                            style={[
                              styles.inputErrContainer,
                              firstKanaNameErrMsg != ""
                                ? { gap: hp("0.95%") }
                                : { gap: 0 },
                            ]}
                          >
                            <TextInput
                              style={[styles.input, styles.inputText]}
                              value={firstKanaName}
                              onChangeText={(text) =>
                                handleInputChange(text, "firstKanaName")
                              }
                              onFocus={() => setBirthDateCalendarVisible(false)}
                            />
                            {firstKanaNameErrMsg != "" && (
                              <HiraginoKakuText style={styles.errText} normal>
                                {firstKanaNameErrMsg}
                              </HiraginoKakuText>
                            )}
                          </View>
                          <View
                            style={[
                              styles.inputErrContainer,
                              lastKanaNameErrMsg != ""
                                ? { gap: hp("0.95%") }
                                : { gap: 0 },
                            ]}
                          >
                            <TextInput
                              style={[styles.input, styles.inputText]}
                              value={lastKanaName}
                              onChangeText={(text) =>
                                handleInputChange(text, "lastKanaName")
                              }
                              onFocus={() => setBirthDateCalendarVisible(false)}
                            />
                            {lastKanaNameErrMsg != "" && (
                              <HiraginoKakuText style={styles.errText} normal>
                                {lastKanaNameErrMsg}
                              </HiraginoKakuText>
                            )}
                          </View>
                        </View>
                      </View>
                      <View style={styles.row}>
                        <View style={styles.labelContainer}>
                          <HiraginoKakuText style={styles.labelText}>
                            生年月日
                          </HiraginoKakuText>
                        </View>
                        <View style={styles.innerRowContainer}>
                          <View style={styles.birthDateInputContainer}>
                            <TextInput
                              ref={birthDateInputRef}
                              style={styles.birthDateInputText}
                              value={birthDate != "" ? format(new Date(birthDate), "yyyy/MM/dd") : birthDate}
                              onPressIn={handleBirthDateCalendarPress}
                              onPointerDown={handleBirthDateCalendarPress}
                              showSoftInputOnFocus={false}
                              onTouchStart={() => Keyboard.dismiss()}
                              editable={false}
                            />
                            <Pressable style={styles.calendarIconContainer}
                              ref={birthDateRef}
                              onPress={handleBirthDateCalendarPress}>
                              <MaterialIcons
                                name="calendar-today"
                                size={22}
                                color={colors.activeCarouselColor}
                                style={styles.calendarIcon}
                              />
                            </Pressable>
                            {isBirthDateCalendarVisible && (
                              <View onLayout={onCalendarLayout} ref={calendarRef} style={styles.calendarContainer}>
                                <CustomCalendar selectedDate={birthDate} onDateSelect={handleBirthDateSelect} />
                              </View>
                            )}
                          </View>
                        </View>
                      </View>
                      <View style={[styles.row, styles.lowerRow]}>
                        <View style={styles.labelContainer}>
                          <HiraginoKakuText style={styles.labelText}>
                            性別
                          </HiraginoKakuText>
                        </View>
                        <View style={styles.genderRadioContainer}>
                          <View
                            style={styles.radioContainer}
                            onLayout={onRadioPanelHandler}
                          >
                            <RadioPanel
                              selected={selectedOption === "M"}
                              onPress={() => handleSelectOption("M")}
                              radioBtnText="男性"
                            />
                          </View>
                          <View style={styles.radioContainer}>
                            <RadioPanel
                              selected={selectedOption === "F"}
                              onPress={() => handleSelectOption("F")}
                              radioBtnText="女性"
                            />
                          </View>
                          <View
                            style={[
                              styles.radioContainer,
                              styles.radioKaitouContainer,
                            ]}
                          >
                            <RadioPanel
                              selected={selectedOption === "N"}
                              onPress={() => handleSelectOption("N")}
                              radioBtnText="回答しない"
                            />
                          </View>
                        </View>
                      </View>
                      <View
                        style={[
                          styles.row, styles.lowerRow,
                          postCodeErrMsg != ""
                            ? { height: "auto" }
                            : { height: hp("5.15%") },
                        ]}
                      >
                        <View style={styles.labelContainer}>
                          <HiraginoKakuText style={styles.labelText}>
                            郵便番号
                          </HiraginoKakuText>
                        </View>
                        <View style={styles.postCodeInputsContainer}>
                          <View
                            style={[
                              styles.inputErrContainer,
                              postCodeErrMsg != ""
                                ? { gap: hp("0.95%") }
                                : { gap: 0 },
                            ]}
                          >
                            <TextInput
                              style={[
                                styles.postCodeInput,
                                styles.postCodeInputText,
                              ]}
                              value={postCode}
                              onChangeText={(text) =>
                                handleInputChange(text, "postCode")
                              }
                              onFocus={() => setBirthDateCalendarVisible(false)}
                            />
                            {postCodeErrMsg != "" && (
                              <HiraginoKakuText style={styles.errText} normal>
                                {postCodeErrMsg}
                              </HiraginoKakuText>
                            )}
                          </View>
                          <Button
                            text="住所検索"
                            onPress={defaultOnPress}
                            style={styles.btnSearch}
                            type="ButtonMSecondary"
                            textSize={16}
                          />
                        </View>
                      </View>
                      <View
                        style={[
                          styles.lastRowContainer,
                          addressErrMsg != ""
                            ? { height: hp("14.5%") }
                            : { height: hp("10.8%") },
                        ]}
                      >
                        <View style={styles.lastRowItem}>
                          <View style={styles.labelContainer}>
                            <HiraginoKakuText style={styles.labelText}>
                              住所
                            </HiraginoKakuText>
                          </View>
                          <View style={styles.addressInputsContainer}>
                            <View
                              style={[
                                styles.inputErrContainer,
                                addressErrMsg != ""
                                  ? { gap: hp("0.95%") }
                                  : { gap: 0 },
                              ]}
                            >
                              <TextInput
                                style={[
                                  styles.addressInput,
                                  styles.addressInputText,
                                ]}
                                value={address}
                                onChangeText={(text) =>
                                  handleInputChange(text, "address")
                                }
                                onFocus={() => setBirthDateCalendarVisible(false)}
                              />
                              {addressErrMsg != "" && (
                                <HiraginoKakuText style={styles.errText} normal>
                                  {addressErrMsg}
                                </HiraginoKakuText>
                              )}
                            </View>
                            <Button
                              text=""
                              type="ButtonMSecondary"
                              style={styles.btnCopy}
                              icon={
                                <MaterialIcons
                                  name="content-copy"
                                  size={24}
                                  color={colors.primary}
                                />
                              }
                              iconPosition="center"
                              onPress={handlePress}
                            />
                          </View>
                        </View>
                        <View style={styles.lastRowBtn}>
                          <View style={styles.labelContainer}></View>
                          <Button
                            text="全員の住所に適用"
                            type="ButtonSGray"
                            style={styles.btnAddress}
                            onPress={handleAddressPress}
                          />
                        </View>
                      </View>
                    </View>
                  </TouchableWithoutFeedback>
                </ScrollView>
              )}

              {isLeftCircleVisible && (
                <View
                  style={{
                    position: "absolute",
                    top: topPosition,
                    left: width > 1194 ? wp("26.1%") : wp("25.6%"),
                  }}
                >
                  <Pressable
                    onPress={OnPreviousCirclePress}
                    style={styles.chevronLeftButton}
                  >
                    <Feather
                      name="chevron-left"
                      size={28}
                      color={colors.secondary}
                    />
                  </Pressable>
                </View>
              )}
              {isRightCircleVisible && (
                <View
                  style={{
                    position: "absolute",
                    top: topPosition,
                    right: -26,
                  }}
                >
                  <Pressable
                    onPress={OnNextCirclePress}
                    style={styles.chevronRightButton}
                  >
                    <Feather
                      name="chevron-right"
                      size={28}
                      color={colors.secondary}
                    />
                  </Pressable>
                </View>
              )}
            </View>
          </View>
        </KeyboardAwareScrollView>
        <Footer
          rightButtonText="確認する"
          hasPreviousButton={false}
          showNextIcon={true}
          onPressNext={handleConfirm}
          hasNextButton={true}
        ></Footer>
        <Toast
          isVisible={toastVisible}
          x={toastX}
          y={toastY}
          text="全員の住所を変更しました"
        />
        <Tooltip
          isVisible={tooltipVisible}
          x={tooltipX}
          y={tooltipY}
          text="コピーしました"
        />
      </SafeAreaView>
    </TouchableWithoutFeedback>
  );
};
const RadioButton = (props: any) => {
  return (
    <View
      style={[
        {
          height: 24,
          width: 24,
          borderRadius: 12,
          borderWidth: 2,
          borderColor: colors.fullyTransparentBlack,
          alignItems: "center",
          justifyContent: "center",
        },
        props.style,
      ]}
    >
      {props.selected ? (
        <View
          style={{
            height: 10,
            width: 10,
            borderRadius: 6,
            backgroundColor: colors.primary,
          }}
        />
      ) : null}
    </View>
  );
};

interface RadioPanelProps {
  selected: boolean;
  onPress: () => void;
  radioBtnText: string;
}

const RadioPanel = ({ selected, onPress, radioBtnText }: RadioPanelProps) => {
  return (
    <Pressable onPress={onPress} style={styles.radioPressable}>
      <View style={styles.radioButtonIcon}>
        <RadioButton
          selected={selected}
          style={[styles.radioButton, selected && styles.selectedRadioButton]}
        />
      </View>
      <View style={styles.radioTextContainer}>
        <HiraginoKakuText style={styles.radioText} normal>
          {radioBtnText}
        </HiraginoKakuText>
      </View>
    </Pressable>
  );
};
export default GroupCheckInEdit;
