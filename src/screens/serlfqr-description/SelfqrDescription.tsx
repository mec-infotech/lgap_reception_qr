import React, { useState, useRef, SetStateAction } from "react";
import {
  StatusBar,
  SafeAreaView,
  View,
  Pressable,
  Image,
  useWindowDimensions,
  FlatList,
  ListRenderItemInfo,
  Animated,
} from "react-native";
import styles from "./SelfqrDescriptionStyles";
import { Header } from "../../components/basics/header";
import { Footer } from "../../components/basics/footer";
import { colors } from "../../styles/color";
import { HiraginoKakuText } from "../../components/StyledText";
import { NavigationProp } from "@react-navigation/native";

type Props = {
  navigation: NavigationProp<any, any>;
};

const slides = [
  {
    id: "1",
    text: "[自治体アプリ]を起動して、自己QRをタップ",
    image: require("../../assets/images/firstCarousel.png"),
  },
  {
    id: "2",
    text: "受付する人を選んで、自己QRを表示するをタップ",
    image: require("../../assets/images/secondCarousel.png"),
  },
  {
    id: "3",
    text: "自己QRが表示されます。読み取りへ進んでください",
    image: require("../../assets/images/thirdCarousel.png"),
  },
];

const Onboarding = ({
  currentIndex,
  setCurrentIndex,
  slides,
}: {
  currentIndex: number;
  setCurrentIndex: React.Dispatch<SetStateAction<number>>;
  scrollToIndex: (index: number) => void;
  slides: any[];
  currentText: string;
  currentImage: any;
}) => {
  const scrollX = useRef(new Animated.Value(0)).current;
  const slidesRef = useRef<FlatList<any> | null>(null);

  const { width } = useWindowDimensions();

  const Item = ({
    item,
    currentIndex,
  }: {
    item: any;
    currentIndex: number;
  }) => {
    const currentText = slides[currentIndex].text;
    const currentImage = slides[currentIndex].image;

    return (
      <View>
        <View style={styles.bodyHeading}>
          <HiraginoKakuText style={styles.bodyHeadingText}>
            {currentText}
          </HiraginoKakuText>
        </View>
        <View style={styles.bodyImageContainer}>
          <Image
            source={currentImage}
            style={[styles.bodyImage, { width, resizeMode: "contain" }]}
          />
        </View>
      </View>
    );
  };

  const _renderItem = (listRenderItemInfo: ListRenderItemInfo<any>) => {
    return <Item item={listRenderItemInfo.item} currentIndex={currentIndex} />;
  };

  const viewableItemsChanged = useRef(
    ({ viewableItems }: { viewableItems: any }) => {
      setCurrentIndex(viewableItems[0]?.index || 0);
    }
  ).current;

  const viewConfig = useRef({ viewAreaCoveragePercentThreshold: 50 }).current;

  return (
    <FlatList
      data={slides}
      renderItem={_renderItem}
      keyExtractor={(item) => item.id}
      horizontal
      showsHorizontalScrollIndicator={false}
      pagingEnabled
      bounces={false}
      onScroll={Animated.event(
        [{ nativeEvent: { contentOffset: { x: scrollX } } }],
        {
          useNativeDriver: false,
        }
      )}
      scrollEventThrottle={32}
      onViewableItemsChanged={viewableItemsChanged}
      viewabilityConfig={viewConfig}
      ref={(ref) => {
        slidesRef.current = ref;
      }}
    />
  );
};

export const SelfqrDescription = ({ navigation }: Props) => {
  const [currentIndex, setCurrentIndex] = useState<number>(0);
  const [currentText, setCurrentText] = useState<string>(slides[0].text);
  const [currentImage, setCurrentImage] = useState<any>(slides[0].image);
  const flatListRef = useRef<FlatList<any> | null>(null);

  const scrollToIndex = (index: number) => {
    flatListRef.current?.scrollToIndex({ animated: true, index: index });
  };

  const scrollToPrevious = () => {
    if (currentIndex > 0) {
      scrollToIndex(currentIndex - 1);
      setCurrentIndex((prevIndex) => {
        const newText = slides[prevIndex - 1].text;
        const newImage = slides[prevIndex - 1].image;
        setCurrentText(newText);
        setCurrentImage(newImage);
        return prevIndex - 1;
      });
    } else if (currentIndex == 0) {
      navigation.navigate("SelectReceptionMethod", {
        userId: "mec",
      });
    }
  };

  const scrollToNext = () => {
    if (currentIndex < slides.length - 1) {
      scrollToIndex(currentIndex + 1);
      setCurrentIndex((prevIndex) => {
        const newText = slides[prevIndex + 1].text;
        const newImage = slides[prevIndex + 1].image;
        setCurrentText(newText);
        setCurrentImage(newImage);
        return prevIndex + 1;
      });
    }
  };

  const handleReadButton = () => {
    navigation.navigate("SelfqrScanner", {
      userId: "mec",
    });
  };

  const handleSelectReceptionMethod = () => {
    navigation.navigate("SelectReceptionMethod", {
      userId: "mec",
    });
  };

  return (
    <SafeAreaView style={styles.mainContainer}>
      <StatusBar barStyle="dark-content" />
      <Header
        titleName="自己QRで受付"
        buttonName="受付をやめる"
        onPress={handleSelectReceptionMethod}
      ></Header>
      <View style={styles.slideContainer}>
        <Onboarding
          currentIndex={currentIndex}
          setCurrentIndex={setCurrentIndex}
          scrollToIndex={scrollToIndex}
          slides={slides}
          currentText={currentText}
          currentImage={currentImage}
        />
      </View>
      <View style={styles.dotView}>
        {slides.map(({}, index: number) => (
          <Pressable
            key={index.toString()}
            style={[
              styles.circle,
              {
                backgroundColor:
                  index === currentIndex
                    ? colors.activeCarouselColor
                    : colors.gray,
              },
            ]}
            onPress={() => scrollToIndex(index)}
          />
        ))}
      </View>
      <Footer
        currentIndex={currentIndex}
        slides={slides}
        hasNextButton={true}
        onPressPrevious={scrollToPrevious}
        onPressNext={scrollToNext}
        onPress={handleReadButton}
      />
    </SafeAreaView>
  );
};
