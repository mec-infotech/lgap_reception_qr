import * as CryptoJS from 'crypto-js';

const keyString = '1234567800000000';
const ivString = '0000000012345678';

// Convert key and IV strings to WordArray
const key = CryptoJS.enc.Utf8.parse(keyString);
const iv = CryptoJS.enc.Utf8.parse(ivString);

export function decryptDecodedText(decodedText: string) {
    try {
        console.log(decodedText, ".....");
        
        let encryptedData;
        
        // Validate the JSON format
        try {
            encryptedData = JSON.parse(decodedText);
        } catch (jsonError) {
            return { error: "Invalid JSON format." };
        }

        // Validate the expected format
        if (!encryptedData.Q2 || !encryptedData.Q1 || !encryptedData.Q3 || !encryptedData.Q4) {
            return { error: "Invalid format: Missing required keys." };
        } else {
            const encryptedStr = encryptedData.Q4;

            const QRjson = CryptoJS.AES.decrypt(encryptedStr, key, {
                iv: iv,
                mode: CryptoJS.mode.CBC,
                padding: CryptoJS.pad.Pkcs7
            }).toString(CryptoJS.enc.Utf8);
    
            if (!QRjson) {
                return { error: "Decryption failed, result is empty." };
            }
    
            return { data: QRjson };
    
        }
        
    } catch (error: unknown) {
        console.error("Error during decryption:", error);
        if (error instanceof Error) {
            return { error: error.message };
        } else {
            return { error: "An unknown error occurred during decryption." };
        }
    }
}
