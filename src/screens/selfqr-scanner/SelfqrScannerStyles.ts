import { Platform, StyleSheet } from "react-native";
import { colors } from "../../styles/color";

export const selfqrScannerstyles = (isScanning: boolean) => {
  return StyleSheet.create({
  mainContainer: {
    width: "100%",
    height: "100%",
  },
  container: {
    flexDirection: "row",
    flex: 1,
    backgroundColor: "#f5f5f5",
  },
  leftSide: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  rightSide: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0, 0, 0, 0.5)",
  },
  overlay: {
    //temporary add
    position: "absolute",
    bottom: 20,
    // ...StyleSheet.absoluteFillObject,
    // backgroundColor: "rgba(0, 0, 0, 0.5)",
    zIndex: 1,
  },
  //temporary add buttonContainer, receptionByOne, receptionByFamily
  buttonContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-evenly",
    width: 597.2,
  },
  webCamContainer: {
    width: 300,
    height: 200,
    position: "relative", 
  },
  receptionByOne: {
    paddingVertical: 8,
    paddingHorizontal: 16,
    borderWidth: 1,
    backgroundColor: colors.secondary,
  },
  receptionByFamily: {
    paddingVertical: 8,
    paddingHorizontal: 16,
    borderWidth: 1,
    backgroundColor: colors.secondary,
  },
  camera: {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: [{ translateX: -180 }, { translateY: -180 }],
    height: 360,
    width: 360,
    zIndex: 1,
  },
  webCamera: {
    position: "absolute", 
    top: "64.5%", 
    left: "50%", 
    width: 360,
    height: 269,
    transform: [{ translateX: -180 }, { translateY: -180 }],
  },
  corner: {
    position: "absolute",
    width: 30,
    height: 30,
    borderWidth: 3,
    borderRadius: 5,
    zIndex: 2,
    borderColor: "transparent",
  },

  topLeftCorner: {
    top: "50%",
    left: "50%",
    marginTop: -182,
    marginLeft: -182,
    borderTopColor: "white",
    borderLeftColor: "white",
  },

  topLeftCornerWeb: {
    top: "54.7%",
    left: "49.5%",
    marginTop: -182,
    marginLeft: -182,
    borderTopColor: "white",
    borderLeftColor: "white",
  },

  topRightCorner: {
    top: "50%",
    left: "50%",
    marginTop: -182,
    marginLeft: 152,
    borderTopColor: "white",
    borderRightColor: "white",
  },

  topRightCornerWeb: {
    top: "54.7%",
    left: "50.6%",
    marginTop: -182,
    marginLeft: 152,
    borderTopColor: "white",
    borderRightColor: "white",
  },

  bottomLeftCorner: {
    top: "50%",
    left: "50%",
    marginTop: 152,
    marginLeft: -182,
    borderBottomColor: "white",
    borderLeftColor: "white",
  },

  bottomLeftCornerWeb: {
    top: "39.3%",
    left: "49.5%",
    marginTop: 152,
    marginLeft: -182,
    borderBottomColor: "white",
    borderLeftColor: "white",
  },

  bottomRightCorner: {
    top: "50%",
    left: "50%",
    marginTop: 152,
    marginLeft: 152,
    borderBottomColor: "white",
    borderRightColor: "white",
  },

  bottomRightCornerWeb: {
    top: "39.3%",
    left: "50.6%",
    marginTop: 152,
    marginLeft: 152,
    borderBottomColor: "white",
    borderRightColor: "white",
  },

  image: {
    width: "100%",
    height: "80%",
    resizeMode: "contain",
    margin: 10,
  },
  messageContainer: {
    position: "absolute",    
    top: isScanning ? '4%' : '3.5%',
    // left: isScanning ? '16.8%' : '20%',
    width: 478,
    ...Platform.select({
      web: {
        top: "8%",
      }
    }),
    zIndex: 1,
    borderRadius: 16,
  },
  scanningMessage: {
    height: 87,
    resizeMode: "contain",
  },
  errorMessage: {
    height: 87,
    resizeMode: "contain",
  },
});
};
