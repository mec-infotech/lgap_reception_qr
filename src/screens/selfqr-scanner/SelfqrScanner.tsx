import React, { useState, useEffect } from "react";
import {
  View,
  Image,
  SafeAreaView,
  StatusBar,
  Text,
  Alert,
  Pressable,
  Platform,
  Modal,
  Button,
} from "react-native";
import { Camera } from "expo-camera";
import { Header } from "../../components/basics/header";
import { Footer } from "../../components/basics/footer";
import { selfqrScannerstyles } from "./SelfqrScannerStyles";
import { NavigationProp, useRoute } from "@react-navigation/native";
import { HiraginoKakuText } from "../../components/StyledText";
import { Html5QrcodeScanType, Html5QrcodeScanner } from 'html5-qrcode';
import { decryptDecodedText } from "./SelfqrScannerService";

type Props = {
  navigation: NavigationProp<any, any>;
};

export const SelfqrScanner = ({ navigation }: Props) => {

  const [hasPermission, setHasPermission] = useState<boolean | null>(null);
  const [scanned, setScanned] = useState(false);
  const [decodedResults, setDecodedResults] = useState('');
  const [isScannerReady, setIsScannerReady] = useState<boolean>(false);
  let qrCodeScanner: Html5QrcodeScanner | undefined;

  const [isScanning, setIsScanning] = useState<boolean | null>(null);
  let styles = selfqrScannerstyles(isScanning as boolean);

  useEffect(() => {
    if (hasPermission && Platform.OS !== 'ios') {
      setIsScannerReady(true);
    }
  }, [hasPermission]);

  useEffect(() => {
    if (isScannerReady) {
      let intervalId;
      let stateCheckIntervalId;
      let scanningTimeout;
      let state = 0;

      const config = {
        fps: 30,
        rememberLastUsedCamera: true,
        supportedScanTypes: [Html5QrcodeScanType.SCAN_TYPE_CAMERA],
      };

      qrCodeScanner = new Html5QrcodeScanner("reader", config, false);
      qrCodeScanner.render(qrCodeSuccessCallback, qrCodeErrorCallback);

      stateCheckIntervalId = setInterval(() => {
        if (qrCodeScanner) {
          state = qrCodeScanner.getState();
        }

        if (state === 2 && !isScanning) {
          setIsScanning(true);
        } else if (state !== 2 && isScanning) {
          setIsScanning(false);
        }
      }, 1000);

      intervalId = setInterval(() => {
        if (state === 2) {
          setIsScanning((prev) => !prev);
        }
      }, 6000);

      scanningTimeout = setTimeout(() => {
        console.log("Scanning took too long. Clearing scanner...");
        qrCodeScanner?.pause();
        setIsScanning(false);
      }, 40000);

      return () => {
        clearInterval(stateCheckIntervalId);
        clearInterval(intervalId);
        if (qrCodeScanner) {
          qrCodeScanner.clear();
        }
      };
    }

    (async () => {
      const { status } = await Camera.requestCameraPermissionsAsync();
      setHasPermission(status === "granted");
    })();
  }, [isScannerReady]);

  const qrCodeSuccessCallback = (decodedText: string) => {
    const decodedText1 = {
      "Q2": 1,
      "Q1": 1,
      "Q3": "712B18CED9105260099B19A90A2A9BFFA78287062B6F4ADFDBBD2043805407E5C7E641BCDE4BF145BB2454D8B10D1DCDF0EAF0FFF3EA2B75CF752BB4EB97AFE7672DA61F0EE53EFBD5275B260E624A28032A6A0E8602A69D77E82E2883EE13A4C37908A9A45451DC5605C04F77F5DEED",
      "Q4": "xV4cuHd/Z8EYFt8b1oFe796ft49xDk0dJpiRsiIdrWst7eohYW9FdvvRo1las9Nh+RoRZ5V1nZMAPAfKL8TQoA2CBUXL8zsDYczfpBjF2lQbwOqcF9UscHWMBkS3OT94NuW0JakleiNh1yX9oi15xRPGG1cwueUaWH8KfncYVzc12ymAudSXMbQ55UPX7IZCPkc2GYLKjVKGvtOpH4B8Mi2mtZqY+Cw/SzDutB+Yam3ErXLrFfnXO4Xmo6qoErwX3edV1czqbESfiv4BzBMv5MzPuuWq4au1Hy5bBkBLhwPlZhpuiArUXTQMKbDCOHnDTCFrWZs6JcCej/kZ0zzgqHv6YzfaGWQJdqeXBcsb6Ij9Mw3iQB5IS+9mnMEbjk4J0NEcJWrw9vH7XRX03T2+YQ0h3xnH6X5IboHqpELIEIIAcXQttN628DB3xeHrwwU/a9kmT7vMbjj0um7yp7mKcJyeBtXx4OgTolozJkqtdGgXjXkVEy1ksEluG4+Pew6SX8/pJICa6zwuBWfRm2AnGMIJ5YPGdBf/cZo4b1nDQKZByb989JtGpdFBP47S4CWXPp5JXrdYAJFt02u5NwTI8Bc25UrDbvAF4gwsSmH4R/NmGeQX0dcQdjwSqyK3TRvhcY8SuUdiVvw+oiJIAMZX7lUOdJBwzT1G9I8A7CDpRlDcmxNfP8JrFzwf9g1zfncM"
    };
    const decodedText2 = JSON.stringify(decodedText1);
    let QRjsonString;

    const result = decryptDecodedText(decodedText);

    if ('error' in result) {
      console.error("Error from tsx:", result.error);
    } else {
      QRjsonString = result.data;

      if (QRjsonString) {
        try {
          const QRjson = JSON.parse(QRjsonString);
          if (QRjson) {
            const fullName = QRjson["1"]["2"];
            const fullNameKana = QRjson["1"]["3"];
            const dateOfBirth = QRjson["1"]["4"];
            const genderCode = QRjson["1"]["5"];
            const postalCode = QRjson["1"]["9"];
            const address = QRjson["1"]["7"];

            // CHANGE Date Format
            const year: string = dateOfBirth.substring(0, 4);
            const month: string = dateOfBirth.substring(4, 6);
            const day: string = dateOfBirth.substring(6, 8);
            const formattedDate: string = `${year}-${month}-${day}`;

            // CHANGE Postal Code Format
            const firstPart: string = postalCode.substring(0, 3);
            const secondPart: string = postalCode.substring(3, 7);
            const formattedPostalCode: string = `${firstPart}-${secondPart}`;

            const [lastName, firstName]: [string, string] = fullName.split(" ");
            const [lastNameKana, firstNameKana]: [string, string] = fullNameKana.split(" ");

            navigation.navigate("CheckInConfirmation", {
              firstName: firstName,
              lastName: lastName,
              firstNameKana: firstNameKana,
              lastNameKana: lastNameKana,
              dateOfBirth: new Date(formattedDate),
              postalCode: formattedPostalCode,
              genderCode: genderCode,
              address: address,
            });

            console.log("Main Person Kanji Name:", fullName);
          }
        } catch (e) {
          console.error("Failed to parse JSON:", e);
        }
      } else {
        console.error("QRjsonString is undefined");
      }
    }

    if (!scanned) {
      if ('error' in result) {
        setIsScanning(false);
        qrCodeScanner?.pause();
      } else {
        console.log(result);
        setScanned(true);
        qrCodeScanner?.clear();
        setIsScanning(null);
      }
    } else {
      setIsScanning(false);
    }

    setDecodedResults(decodedText);
  };


  const qrCodeErrorCallback = (errorMessage: string) => {
    const err = errorMessage;
    console.log(err);
  };


  const handleBarcodeScanned = ({ data }: { data: string }) => {
    if (!scanned) {
      setScanned(true);
      Alert.alert(
        "QR Code Scanned",
        `Bar code with data ${data} has been scanned!`,
        [
          {
            text: "OK",
            onPress: () => {
              setScanned(false);
            },
          },
        ],
        { cancelable: false }
      );
    }
  };

  if (hasPermission === null) {
    return <Text>Requesting camera permission...</Text>;
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>;
  }

  const handleReturnButton = () => {
    navigation.navigate("SelfqrDescription", {
      userId: "mec",
    });
  };

  const handleCheckInConfirmation = () => {
    // const checkInConfirmationParams = new CheckInConfirmationParams();
    // checkInConfirmationParams.user.userId = "user03";
    // checkInConfirmationParams.eventId = 3;
    // checkInConfirmationParams.venueId = 4;

    // checkInConfirmationParams.entrantRecord.originalEntrant.receptionTypeCode = "1";
    // checkInConfirmationParams.entrantRecord.originalEntrant.firstName = "太郎";
    // checkInConfirmationParams.entrantRecord.originalEntrant.lastName = "出茂";
    // checkInConfirmationParams.entrantRecord.originalEntrant.firstNameKana = "タロウ";
    // checkInConfirmationParams.entrantRecord.originalEntrant.lastNameKana = "イズモ";
    // checkInConfirmationParams.entrantRecord.originalEntrant.dateOfBirth = new Date("2000-01-02");
    // checkInConfirmationParams.entrantRecord.originalEntrant.postalCode = "515-0004";
    // checkInConfirmationParams.entrantRecord.originalEntrant.genderCode = "2";
    // checkInConfirmationParams.entrantRecord.originalEntrant.address = "三重県松阪市なんとか町11-2　マンション名102あああああああああああああああああ";
    // checkInConfirmationParams.entrantRecord.modifiedEntrant = checkInConfirmationParams.entrantRecord.originalEntrant;

    console.log(isScanning, "here we know2");
    console.log(qrCodeScanner?.getState());
    // navigation.navigate("CheckInConfirmation", {
    //   checkInConfirmationParams
    // });
  };

  const handleGroupCheckInConfirmation = () => {
    navigation.navigate("GroupCheckInConfirmation", {
      userId: "mec",
    });
  };

  const handleSelectReceptionMethod = () => {
    navigation.navigate("SelectReceptionMethod", {
      userId: "mec",
    });
  };

  const closeModal = () => {
    setScanned(false);
  };

  return (
    <SafeAreaView style={styles.mainContainer}>
      <StatusBar barStyle="dark-content" />
      <Header
        titleName="自己QRをかざしてください"
        buttonName="受付をやめる"
        onPress={handleSelectReceptionMethod}
      />
      <View style={styles.container}>
        <View style={styles.leftSide}>
          <Image
            source={require("../../assets/images/qrScanner.png")}
            style={styles.image}
          />
        </View>
        {hasPermission && (
          Platform.OS === 'ios' ? (

            <View style={styles.rightSide}>
              <View style={styles.messageContainer}>
                {isScanning && (
                  <Image
                    source={require("../../assets/images/qr_inprocess.png")}
                    style={styles.scanningMessage}
                  />
                )}

                {isScanning === false && (
                  <Image
                    source={require("../../assets/images/qr_fail.png")}
                    style={styles.errorMessage}
                  />
                )}
              </View>
              <Camera
                style={styles.camera}
                type={"back" as any}
                onBarCodeScanned={handleBarcodeScanned}
              />

              <View style={[styles.corner, styles.topLeftCorner]} />
              <View style={[styles.corner, styles.topRightCorner]} />
              <View style={[styles.corner, styles.bottomLeftCorner]} />
              <View style={[styles.corner, styles.bottomRightCorner]} />
              <View style={styles.overlay}>
                <View style={styles.buttonContainer}>
                  <Pressable
                    style={styles.receptionByOne}
                    onPress={handleCheckInConfirmation}
                  >
                    <HiraginoKakuText normal>1人で受付</HiraginoKakuText>
                  </Pressable>
                  <Pressable
                    style={styles.receptionByFamily}
                    onPress={handleGroupCheckInConfirmation}
                  >
                    <HiraginoKakuText normal>家族で受付</HiraginoKakuText>
                  </Pressable>
                </View>
              </View>
            </View>
          ) : (
            <View style={styles.rightSide}>

              <View style={styles.messageContainer}>
                {isScanning && (
                  <Image
                    source={require("../../assets/images/qr_inprocess.png")}
                    style={styles.scanningMessage}
                  />
                )}

                {isScanning === false && (
                  <Image
                    source={require("../../assets/images/qr_fail.png")}
                    style={styles.errorMessage}
                  />
                )}
              </View>
              <View style={styles.webCamContainer}>
                <View id="reader" style={styles.webCamera} />
              </View>

              <View style={[styles.corner, styles.topLeftCornerWeb]} />
              <View style={[styles.corner, styles.topRightCornerWeb]} />
              <View style={[styles.corner, styles.bottomLeftCornerWeb]} />
              <View style={[styles.corner, styles.bottomRightCornerWeb]} />
              <View style={styles.overlay}>
                <View style={styles.buttonContainer}>
                  <Pressable
                    style={styles.receptionByOne}
                    onPress={handleCheckInConfirmation}
                  >
                    <HiraginoKakuText normal>1人で受付</HiraginoKakuText>
                  </Pressable>
                  <Pressable
                    style={styles.receptionByFamily}
                    onPress={handleGroupCheckInConfirmation}
                  >
                    <HiraginoKakuText normal>家族で受付</HiraginoKakuText>
                  </Pressable>
                </View>
              </View>
            </View>
          )
        )}

      </View>
      <Footer
        hasNextButton={false}
        onPressPrevious={handleReturnButton}
      ></Footer>
    </SafeAreaView>
  );
};
